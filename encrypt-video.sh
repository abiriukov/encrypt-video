#!/bin/sh

# Encypt a file with a passphrase

# Encrypt the file
echo "Encrypting " $1
gpg --batch -c --passphrase-file ~/.passphrase $1
